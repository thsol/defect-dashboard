package com.tiaa.coe;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DefectDashboard
 */
@WebServlet("/DefectDashboard")
public class DefectDashboard extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DefectDashboard() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println(request.getParameter("project"));
		System.out.println(request.getParameter("startDate"));
		System.out.println(request.getParameter("endDate"));
		
		ArrayList<DefectDetailsBO> defects = new ArrayList<DefectDetailsBO>();
		defects = ALMConnectAPI.getAllDefectWithinRange();
		System.out.println("in servelet :: "+defects.size());
		
		/*
		 * request.setAttribute("defects", defects); RequestDispatcher rd=
		 * request.getRequestDispatcher("defects.jsp"); rd.forward(request, response);
		 * //rd.forward(request, response); doGet(request,response);
		 * System.out.println("Servelet Call Completed");
		 */
		
		request.setAttribute("defects", defects);
		RequestDispatcher rd = request.getRequestDispatcher("defects.jsp");
		rd.forward(request, response);
		
		
	}

}
